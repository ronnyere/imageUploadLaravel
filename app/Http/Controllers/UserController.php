<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function getAccount()
    {
        return view('account', ['user' => Auth::user()]);
    }

    public function postSaveAccount(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:100'
        ]);

        $user           = Auth::user();
        $old_email      = $user->email;
        $user->email    = $request['email'];

        $user->update();
        $file = $request->file('image');

        $file_email     = $request['email'] . '-' . $user->id . '.jpg';
        $old_file_email = $old_email . '-' . $user->id . '.jpg';
        $update = false;

        if (Storage::disk('local')->has($old_file_email)) {
            $old_file = Storage::disk('local')->get($old_file_email);
            Storage::disk('local')->put($file_email, $old_file);
            $update = true;
        }

        if ($file) {
            Storage::disk('local')->put($file_email, File::get($file));
        }

        if ($update && $old_file_email !== $file_email) {
            Storage::delete($old_file_email);
        }
        return redirect()->route('account');
    }

    public function getUserImage()
    {
        $user = Auth::user();
        $image ="". $user->email . '-' . $user->id . '.jpg';
        $file = Storage::disk('local')->get($image);
        return Response::make($file,200,[ 'Content-Type' => 'image/jpeg']);


    }

}