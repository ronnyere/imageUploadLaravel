<?php

namespace App\Http\Middleware;

use Closure;

class CheckImageUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->id == $request->file_email)
        {
            abort(404); // não autorizado.
        }

        return $next($request);
    }
}
